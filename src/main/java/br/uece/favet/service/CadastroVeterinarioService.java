package br.uece.favet.service;

import java.io.Serializable;

import javax.inject.Inject;

import br.uece.favet.model.Veterinario;
import br.uece.favet.repository.VeterinarioRepository;
import br.uece.favet.util.jpa.Transactional;

public class CadastroVeterinarioService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	VeterinarioRepository veterinarioRepository;
	
	@Transactional
	public Veterinario salvar(Veterinario veterinario){

		try {
			veterinario = this.veterinarioRepository.guardar(veterinario);
		} catch (NegocioException e) {
			throw new NegocioException("Erro ao tentar salvar Veterinário: ");
		}

		return veterinario;
	}

}
