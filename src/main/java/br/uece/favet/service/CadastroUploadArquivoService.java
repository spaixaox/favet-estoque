package br.uece.favet.service;

import java.io.Serializable;

import javax.inject.Inject;

import br.uece.favet.model.UploadArquivo;
import br.uece.favet.repository.UploadArquivoRepository;
import br.uece.favet.util.jpa.Transactional;

public class CadastroUploadArquivoService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	UploadArquivoRepository uploadArquivoRepository;

	@Transactional
	public UploadArquivo salvar(UploadArquivo uploadArquivo){
	
		uploadArquivo = this.uploadArquivoRepository.guardar(uploadArquivo);
		return uploadArquivo;
		
	}
}
