package br.uece.favet.service;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Inject;

import br.uece.favet.model.Usuario;
import br.uece.favet.repository.UsuarioRepository;
import br.uece.favet.util.jpa.Transactional;

public class CadastroUsuarioService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	UsuarioRepository usuarioRepository;
	
	@Transactional
	public Usuario salvar(Usuario usuario){

		try {
 		    usuario.setDataHoraAlteracao(new Date());
			usuario = this.usuarioRepository.guardar(usuario);
		} catch (NegocioException e) {
			throw new NegocioException("Erro ao tentar salvar Usuário: ");
		}

		return usuario;
	}

}
