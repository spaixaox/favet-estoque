package br.uece.favet.service;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Inject;

import br.uece.favet.model.Animal;
import br.uece.favet.model.Proprietario;
import br.uece.favet.repository.ProprietarioRepository;
import br.uece.favet.util.jpa.Transactional;

public class CadastroProprietarioService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	ProprietarioRepository proprietarioRepository;
	

	@Transactional
	public Proprietario salvarEspecial(Proprietario proprietario, Animal animalEdicao){

		if (proprietario.isNovo()){
			proprietario.setDataCriacao(new Date());
			proprietario.setDataHoraCriacao(new Date());			
		}

		/*		
		animalEdicao.setProprietario(proprietario);
		proprietario.getAnimais().add(animalEdicao);		
	*/
		try {
			proprietario = this.proprietarioRepository.guardar(proprietario);
			proprietario = proprietarioRepository.porId(proprietario.getId());			
		} catch (Exception e) {
			throw new NegocioException("Erro ao tentar saltar Proprietário/Animal: "+e.getMessage());
		}

		
		return proprietario;
		
	}

	@Transactional
	public Proprietario salvarSemAnimais(Proprietario proprietario){
		if (proprietario.isNovo()){
			proprietario.setDataCriacao(new Date());
		}
		
		try {
			proprietario = this.proprietarioRepository.guardar(proprietario);			
		} catch (Exception e) {
			throw new NegocioException("Erro ao tentar saltar Proprietário/Animal: "+e.getMessage());
		}
		return proprietario;		
	}
	
	@Transactional
	public Proprietario salvar(Proprietario proprietario){
		
		if (proprietario.isNovo()){
			proprietario.setDataCriacao(new Date());
		}
		
		if (proprietario.getAnimais() == null || proprietario.getAnimais().isEmpty()) {
				throw new NegocioException("O Proprietário deve possuir pelo menos um animal cadastrado.");			
    	} else {
			proprietario = this.proprietarioRepository.guardar(proprietario);			
		}
		
		return proprietario;
		
	}
}
