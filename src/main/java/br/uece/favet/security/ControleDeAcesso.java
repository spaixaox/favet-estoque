package br.uece.favet.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//https://github.com/algaworks/ebook-javaee/blob/master/src/main/java/com/algaworks/financeiro/filter/AutorizacaoFilter.java

@WebFilter("*.xhtml")
//@WebFilter(servletNames = {"Faces Servlet"})
public class ControleDeAcesso implements Filter {
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();

		if ((session.getAttribute("USUARIOLogado") != null)
				|| (req.getRequestURI().endsWith("Login.xhtml"))
				|| (req.getRequestURI().endsWith("Erro.xhtml"))
				|| (req.getRequestURI().endsWith("*.css"))
				|| (req.getRequestURI().contains("javax.faces.resources/"))) {
				
			chain.doFilter(request, response);

				/*
			if (session.getAttribute("USUARIOLogado") != null){
				
			}
				if (req.getRequestURI().endsWith("Proprietarios.xhtml")
				  || req.getRequestURI().endsWith("PesquisaProprietarios.xhtml")
				  || req.getRequestURI().endsWith("PesquisaProprietariosSemAnimais.xhtml")
				  || req.getRequestURI().endsWith("PesquisaAtendimentoPacientes.xhtml")
				  || req.getRequestURI().endsWith("AtendimentoPacientes.xhtml")){
					String grupoUsuario = session.getAttribute("GRUPOUSUARIOLogado").toString().toUpperCase(); 
					if (grupoUsuario.equals("ADMINISTRADOR")
						|| grupoUsuario.equals("VETERINARIO")
						|| grupoUsuario.equals("GESTOR")
						|| grupoUsuario.equals("ASSISTENTE_ATENDIMENTO")) {
			 			chain.doFilter(request, response);
						//redireciona("/Favet/proprietarios/Proprietarios.xhtml", response);		
					} else {
						redireciona("/Favet/AcessoNegado.xhtml", response);		
					}
			}
			*/
		}

		else {
			redireciona("/Favet/Login.xhtml", response);			
		}
        
        
        /*
        
        if (session.getAttribute("USUARIOLogado") == null 
        		&& !req.getRequestURI().endsWith("/Login.xhtml")
				&& !req.getRequestURI().contains("/javax.faces.resource/")) {
        	redireciona("/Favet/Login.xhtml", response);
		} else {
 			chain.doFilter(request, response);
		}        
	*/
        
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		
	}

	public void destroy() {
	}

	private void redireciona(String url, ServletResponse response)
			throws IOException {
		HttpServletResponse res = (HttpServletResponse) response;
		res.sendRedirect(url);
	}
}