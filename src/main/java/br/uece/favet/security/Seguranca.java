package br.uece.favet.security;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import br.uece.favet.model.Usuario;
import br.uece.favet.model.Veterinario;
import br.uece.favet.util.jsf.SessionUtil;

@Named
@RequestScoped
public class Seguranca {
	
	private Usuario usuarioLogado;
	
	public Seguranca() {
		if (usuarioLogado == null){
			this.usuarioLogado = getUsuarioLogado();	
		}
	}

	public String getNomeUsuario() {
		String nome = null;
		
		if (usuarioLogado != null) {
			nome = usuarioLogado.getNome();
		}
		
		return nome;
	}
	
	public Veterinario getVeterinarioUsuario() {
		Veterinario veterinario = null;
		
		if (usuarioLogado != null) {
			veterinario = usuarioLogado.getVeterinario();
		}
		
		return veterinario;
	}
	
	public Usuario getUsuario() {
		return this.usuarioLogado;
	}
	
	
	public String getTipoGrupoUsuario() {
		String nomeTipoGrupo = null;
		
		if (usuarioLogado != null && usuarioLogado.getGrupo() != null) {
			nomeTipoGrupo = usuarioLogado.getGrupo().getTipoGrupoUsuario().toString().toUpperCase();
		}
		
		return nomeTipoGrupo;
	}
	
	public boolean isUsuarioPodeIniciarAtendimento(){
		try {
			if (this.usuarioLogado != null){
				return this.usuarioLogado.getPodeIniciarAtendimento();			
			} else{
				return false;
			}			
		} catch (Exception e) {
			return false;
		}
	}
	
	private Usuario getUsuarioLogado() {
		Usuario usuario = null;

		try {
			usuario = ((Usuario)SessionUtil.getParam("USUARIOLogado"));	
		} catch (Exception e) {
			System.out.println("Erro ao tentar recuperar o usuário logado [Seguranca->getUsuarioLogado()]");
		}
		
		return usuario;
	}
}