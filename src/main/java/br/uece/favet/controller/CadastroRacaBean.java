package br.uece.favet.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.uece.favet.model.Especie;
import br.uece.favet.model.Raca;
import br.uece.favet.model.enums.TipoPorte;
import br.uece.favet.repository.EspecieRepository;
import br.uece.favet.service.CadastroRacaService;
import br.uece.favet.service.NegocioException;
import br.uece.favet.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroRacaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Raca raca;
	private List<Especie> especies;
	
	@Inject
	private CadastroRacaService cadastroRacaService;
	
	@Inject
	private EspecieRepository especieRepository;	
	
	public Raca getRaca() {
		return raca;
	}

	public void setRaca(Raca raca) {
		this.raca = raca;
	}

	public List<Especie> getEspecies() {
		return especies;
	}

	public TipoPorte[] getTipoPorte(){
	     return TipoPorte.values();
	}	
	
	
	public CadastroRacaBean() {
        limpar();
	}

	public void limpar() {
		raca = new Raca();
	}

	public void salvar() {

		try {

	    	this.raca = this.cadastroRacaService
							.salvar(this.raca);
		    	
			FacesUtil.addInfoMessage("Raça salva com sucesso!");

		} catch (NegocioException ne) {
			FacesUtil.addErrorMessage("Erro ao Salvar Raça 1: "+ne.getMessage());
		} 	catch (Exception e){
			FacesUtil.addErrorMessage("Erro ao Salvar Raça 2: "+e.getMessage()+", Causa :"+e.getCause());
		} 
		
	}
	
	public void inicializar() {
		if (FacesUtil.isNotPostback()) {
			this.especies = this.especieRepository.especies();			
		}
	}
	
	public boolean isEditando() {
		return this.raca.getId() != null;
	}
	
}
