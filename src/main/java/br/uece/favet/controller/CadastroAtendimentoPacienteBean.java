package br.uece.favet.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.primefaces.event.FileUploadEvent;

import br.uece.favet.model.Animal;
import br.uece.favet.model.AtendimentoPaciente;
import br.uece.favet.model.Configuracao;
import br.uece.favet.model.Consultorio;
import br.uece.favet.model.Patologia;
import br.uece.favet.model.UploadArquivo;
import br.uece.favet.model.Usuario;
import br.uece.favet.model.Veterinario;
import br.uece.favet.model.enums.StatusAtendimentoPaciente;
import br.uece.favet.model.enums.TipoAtendimentoPaciente;
import br.uece.favet.repository.AnimalRepository;
import br.uece.favet.repository.AtendimentoPacienteRepository;
import br.uece.favet.repository.ConfiguracaoRepository;
import br.uece.favet.repository.ConsultorioRepository;
import br.uece.favet.repository.PatologiaRepository;
import br.uece.favet.repository.UploadArquivoRepository;
import br.uece.favet.repository.VeterinarioRepository;
import br.uece.favet.security.Seguranca;
import br.uece.favet.service.CadastroAtendimentoPacienteService;
import br.uece.favet.service.NegocioException;
import br.uece.favet.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroAtendimentoPacienteBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private AtendimentoPaciente atendimentoPaciente;

	private List<UploadArquivo> uploadArquivos;	
	private List<Patologia> patologias;	
	private List<Veterinario> veterinarios;
	private List<Consultorio> consultorios;
	private List<AtendimentoPaciente> historicoAtendimentosPaciente;
	private List<Configuracao> configuracoes;
	private String anamnese;
	private String receituario;
	private UploadArquivo uploadArquivo;	

	@Inject
	private Seguranca seguranca;
	@Inject
	private AutenticadorBean autenticadorBean; 
	
	@Inject
	private AnimalRepository animalRepository;

	@Inject
	private ConfiguracaoRepository configuracaoRepository;
	
	@Inject
	private AtendimentoPacienteRepository atendimentoPacienteRepository;	
	
	@Inject
	private PatologiaRepository patologiaRepository;

	@Inject
	private VeterinarioRepository veterinarioRepository;

	@Inject
	private ConsultorioRepository consultorioRepository;

	@Inject
	private CadastroAtendimentoPacienteService cadastroAtendimentoPacienteService;
	
	@Inject
	private UploadArquivoRepository uploadArquivoRepository;
	
	@Inject
	private ArquivoBean arquivoBean;	

	//private String dadosAnimal;

	public AtendimentoPaciente getAtendimentoPaciente() {
		return atendimentoPaciente;
	}
	
	public UploadArquivo getUploadArquivo() {
		return uploadArquivo;
	}

	public List<UploadArquivo> getUploadArquivos() {
		return uploadArquivos;
	}

	public void setAtendimentoPaciente(
			AtendimentoPaciente atendimentoPaciente) {
		this.atendimentoPaciente = atendimentoPaciente;
	}

	public List<Veterinario> getVeterinarios() {
		return veterinarios;
	}

	public void setVeterinarios(List<Veterinario> veterinarios) {
		this.veterinarios = veterinarios;
	}

	public List<Consultorio> getConsultorios() {
		return consultorios;
	}

	public void setConsultorios(List<Consultorio> consultorios) {
		this.consultorios = consultorios;
	}
	
	public List<Patologia> getPatologias() {
		return patologias;
	}

	public void setPatologias(List<Patologia> patologias) {
		this.patologias = patologias;
	}
	
	public List<AtendimentoPaciente> getHistoricoAtendimentosPaciente() {
		return historicoAtendimentosPaciente;
	}

	public void setHistoricoAtendimentosPaciente(
			List<AtendimentoPaciente> historicoAtendimentosPaciente) {
		this.historicoAtendimentosPaciente = historicoAtendimentosPaciente;
	}
	
	public List<Configuracao> getConfiguracoes() {
		return configuracoes;
	}

	public void setConfiguracoes(List<Configuracao> configuracoes) {
		this.configuracoes = configuracoes;
	}
	
	public String getAnamnese() {
		return anamnese;
	}

	public void setAnamnese(String anamnese) {
		this.anamnese = anamnese;
	}

	public String getReceituario() {
		return receituario;
	}

	public void setReceituario(String receituario) {
		this.receituario = receituario;
	}

	public CadastroAtendimentoPacienteBean() {
		limpar();
	}

	public StatusAtendimentoPaciente[] getStatusAtendimentoPaciente(){
	     return StatusAtendimentoPaciente.values();
	}

	public TipoAtendimentoPaciente[] getTipoAtendimentoPaciente(){
	     return TipoAtendimentoPaciente.values();
	}	
	
	/*
	public String getDadosAnimal() {
		return dadosAnimal;
	}

	public void setDadosAnimal(String dadosAnimal) {
		this.dadosAnimal = dadosAnimal;
	}
	*/

	private void carregarConsultorioDaSessao(){
		if (autenticadorBean.getConsultorio() != null && this.atendimentoPaciente.isEmEspera() ){
			this.atendimentoPaciente.setConsultorio(autenticadorBean.getConsultorio());
		}
	}
	
	private void salvarConsultorioNaSessao(){
		if (this.atendimentoPaciente.getConsultorio() != null &&
				autenticadorBean.getConsultorio() != this.atendimentoPaciente.getConsultorio()) {
				autenticadorBean.setConsultorio(this.atendimentoPaciente.getConsultorio());
		}
	}
	
	private void carregarVeterinarioLogado(){
		if (this.isUsuarioVeterinario() && this.atendimentoPaciente.isEmEspera()){
				if (seguranca.getVeterinarioUsuario() != null){
					this.atendimentoPaciente.setVeterinario(seguranca.getVeterinarioUsuario());					
				}			
			} 			
	}

    public void upload(FileUploadEvent event) {
    	//uploadArquivo.setDescricao(descricaoArquivo);
    	System.out.println("uploadArquivo.getDescricao(): "+uploadArquivo.getDescricao());
    	arquivoBean.upload(event, this.atendimentoPaciente, uploadArquivo);
    	listarArquivos();
    	uploadArquivo = new UploadArquivo();
    }	
	
    public void listarArquivos() {
    	this.uploadArquivos = this.uploadArquivoRepository.porAtendimento(this.atendimentoPaciente.getId());
    }
    
    public boolean existeArquivo(String caminhoDiretorio, String nomeArquivo){
    	return arquivoBean.existeArquivo(caminhoDiretorio, nomeArquivo);
    }
    
	public boolean naoExisteArquivo(String caminhoDiretorio, String nomeArquivo){
		return !existeArquivo(caminhoDiretorio, nomeArquivo);
    }    
	
	public void inicializar() {
		if (FacesUtil.isNotPostback()) {
			carregarPatologias();
			this.veterinarios = this.veterinarioRepository.veterinarios();
			this.consultorios = this.consultorioRepository.consultorios();
			Long idAnima = this.atendimentoPaciente.getAnimal().getId();
			this.historicoAtendimentosPaciente = this.atendimentoPacienteRepository.porAnimal(idAnima);
			this.configuracoes = this.configuracaoRepository.configuracoes();
            carregarVeterinarioLogado();
            carregarConsultorioDaSessao();
			listarArquivos();            
		}
	}

	public void limpar() {
		atendimentoPaciente = new AtendimentoPaciente();
		uploadArquivo = new UploadArquivo();

	}

	public void carregarPatologias(){
		if (this.atendimentoPaciente.getAnimal() != null){
			this.patologias = this.patologiaRepository.porEspecie(this.atendimentoPaciente.getAnimal().getEspecie().getId());
		}
	}
	
	public void salvar() {
		Usuario usuarioLogado = null;
		
		System.out.println("dataNascimento: "+this.atendimentoPaciente.getAnimal().getDataNascimento());
		
		try {
			usuarioLogado = seguranca.getUsuario();
		} catch (Exception e) {
			System.out.println("Erro ao tentar recuperar o usuário logado [CadastroAtendimentoPacienteBean->salvar()]");
		}
		
		try {
			this.atendimentoPaciente = this.cadastroAtendimentoPacienteService
					.salvar(this.atendimentoPaciente, usuarioLogado);
			
			if (this.atendimentoPaciente.isEmEspera() || this.atendimentoPaciente.isEmAtendimento()){
				this.salvarConsultorioNaSessao();				
			}
			
			FacesUtil.addInfoMessage("Atendimento salvo com sucesso!");
		} catch (Exception e) {
			FacesUtil.addErrorMessage("Atendimento NÃO foi salvo. Mensagem: "+e.getMessage()+e.getStackTrace().toString()+e.getCause());
		}
	}
	
	public void cancelarAtendimento() {
		try {
			this.atendimentoPaciente.setDataHoraCancelamento(new Date());
			this.atendimentoPaciente.setStatus(StatusAtendimentoPaciente.CANCELADO);
			salvar();
			FacesUtil.addInfoMessage("Atendimento cancelado com sucesso!");			
		} catch (Exception e) {
			FacesUtil.addErrorMessage("Atendimento não foi cancelado. Mensagem: "+e.getMessage());
		}
	}
	
	private void iniciarCamposAtendimento(){
		if (this.atendimentoPaciente.isEmEspera()){
			
			try {
				for (Configuracao configuracao : configuracoes) {
					if (configuracao.getChave().equals("ATENDIMENTO_ANAMNESE") && this.atendimentoPaciente.getAnamnese().isEmpty()){
						this.atendimentoPaciente.setAnamnese(configuracao.getValor());
					} else if (configuracao.getChave().equals("ATENDIMENTO_RECEITUARIO")){
						this.atendimentoPaciente.setReceituario(configuracao.getValor());
					} else if (configuracao.getChave().equals("ATENDIMENTO_AVALIACAO_FISICA") && this.atendimentoPaciente.getExameFisico().isEmpty()) {
						this.atendimentoPaciente.setExameFisico(configuracao.getValor());					
					} else if (configuracao.getChave().equals("ATENDIMENTO_LAUDO_ULTRASSOM") &&  StringUtils.isBlank(this.atendimentoPaciente.getLaudoUltrassom())  && isTipoAtendimentoExame()) {					
						this.atendimentoPaciente.setLaudoUltrassom(configuracao.getValor());					
	  			    }
	 			}
			} catch (Exception e) {
				throw new NegocioException("Erro ao tentar iniciar Campos de Atendimento");
			}
			
		}
	}
	
	private Usuario getUsuarioLogado(){
		Usuario usuarioLogado = null;
		
		try {
			usuarioLogado = seguranca.getUsuario();
		} catch (Exception e) {
			System.out.println("Erro ao tentar pegar usuário logado na Sessão");
		}	
		
		return usuarioLogado;
	}
	
	public void iniciarAtendimento() {

		Usuario usuarioLogado = this.getUsuarioLogado();
		
		try {
			  if(this.atendimentoPaciente.isPodeIniciar()){
					this.atendimentoPaciente.setDataHoraInicio(new Date());
					iniciarCamposAtendimento();
					this.atendimentoPaciente.setStatus(StatusAtendimentoPaciente.ATENDIMENTO);
					
					if (usuarioLogado != null){
						this.atendimentoPaciente.setUsuarioIniciou(usuarioLogado);
					}
					
					salvar();
					FacesUtil.addInfoMessage("Atendimento iniciado com sucesso.");
					System.out.println("Atendimento Iniciado");
			  }
		} catch (Exception e) {
			FacesUtil.addErrorMessage("Atendimento não foi iniciado. Mensagem: "+e.getMessage());			
		}

	}
	
	public void finalizarAtendimento() {

		Usuario usuarioLogado = this.getUsuarioLogado();
		
		try {
			if (this.atendimentoPaciente.isPodeFinalizar()){
				this.atendimentoPaciente.setDataHoraFim(new Date());
				this.atendimentoPaciente.setStatus(StatusAtendimentoPaciente.ATENDIDO);
				
				if (usuarioLogado != null){
					this.atendimentoPaciente.setUsuarioFinalizou(usuarioLogado);
				}
				
				salvar();
				FacesUtil.addInfoMessage("Atendimento finalizado com sucesso.");
				System.out.println("Atendimento Finalizado");
			}
			
		} catch (Exception e) {
			FacesUtil.addErrorMessage("Atendimento não foi finalizado. Mensagem: "+e.getMessage());
			System.out.println("Atendimento não foi Finalizado"+e.getMessage());
		}
		
	}
	
	public List<Animal> completarAnimal(String nome) {
		return this.animalRepository.porNome(nome);
	}
	
	public boolean isEditando() {
		return this.atendimentoPaciente.getId() != null;
	}
	
	public boolean isUsuarioVeterinario() {
		return seguranca.getTipoGrupoUsuario().toUpperCase().equals("VETERINARIO");
	}
	
	public boolean isTipoAtendimentoExame(){
		return this.atendimentoPaciente.getTipoAtendimentoPaciente().equals(TipoAtendimentoPaciente.EXAME);
	}

	private boolean isHabilitarBotaoIniciar(){
		boolean habilita = false;
		
		if (this.atendimentoPaciente.isEmEspera()){
		    if (isUsuarioVeterinario() || 
		    	(!isUsuarioVeterinario() && seguranca.isUsuarioPodeIniciarAtendimento()) ){
		    	habilita = true;
		    }
		}
		
		return habilita;
	}
		
	public boolean isDesabilitarBotaoIniciar(){
		return !isHabilitarBotaoIniciar();
	}

	private boolean isHabilitarBotaoFinalizar(){
		boolean habilita = false;
		
		if (this.atendimentoPaciente.isEmAtendimento()){
		    if (isUsuarioVeterinario() || 
		    	(!isUsuarioVeterinario() && seguranca.isUsuarioPodeIniciarAtendimento()) ){
		    	habilita = true;
		    }
		}
		
		return habilita;
	}

	public boolean isDesabilitarBotaoFinalizar(){
		return !isHabilitarBotaoFinalizar();
	}
	
}
