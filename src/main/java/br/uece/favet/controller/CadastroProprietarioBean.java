package br.uece.favet.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.uece.favet.model.Animal;
import br.uece.favet.model.Endereco;
import br.uece.favet.model.Especie;
import br.uece.favet.model.Proprietario;
import br.uece.favet.model.Raca;
import br.uece.favet.repository.EspecieRepository;
import br.uece.favet.repository.RacaRepository;
import br.uece.favet.security.Seguranca;
import br.uece.favet.service.CadastroProprietarioService;
import br.uece.favet.service.NegocioException;
import br.uece.favet.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroProprietarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Proprietario proprietario;
	private Animal animalEdicao;
	
	private Animal animalSelecionado;
	
	@Inject
	private Seguranca seguranca;	
	
	@Inject
	private CadastroProprietarioService cadastroProprietarioService;
	
	@Inject
	private EspecieRepository especieRepository;
	
	@Inject
	private RacaRepository racaRepository;

	private List<Especie> especies;
	private List<Raca> racas;	
	
	public Animal getAnimalEdicao() {
		return animalEdicao;
	}

	public void setAnimalEdicao(Animal animalEdicao) {
		this.animalEdicao = animalEdicao;
	}
	
	public Animal getAnimalSelecionado() {
		return animalSelecionado;
	}

	public void setAnimalSelecionado(Animal animalSelecionado) {
		this.animalSelecionado = animalSelecionado;
	}

	public Proprietario getProprietario() {
		return proprietario;
	}

	public void setProprietario(Proprietario proprietario) {
		this.proprietario = proprietario;
	}

	public List<Especie> getEspecies() {
		return especies;
	}

	public List<Raca> getRacas() {
		return racas;
	}

	public void setRacas(List<Raca> racas) {
		this.racas = racas;
	}

	public CadastroProprietarioBean() {
		limpar();
	}
	
	public void limpar() {
		proprietario = new Proprietario();
		proprietario.setEndereco(new Endereco());
		this.animalEdicao = new Animal();
	}

	/*
	public void editarAnimal(int indiceLinha) {
		this.animalEdicao = this.proprietario.getAnimais().get(indiceLinha);
		carregarRacasPorEspecie();
	}
	*/
	public void editarAnimal(Animal animal) {
		this.animalEdicao = animal;
		System.out.println("Animal Selecionado: animal: "+animal+" id: "+this.animalEdicao.getId());
		System.out.println("nome: "+ this.animalEdicao.getNome());
		carregarRacasPorEspecie();
	}	

	
	public void visualizarAnimal(Animal animal) {
		this.animalEdicao = animal;
		System.out.println("Animal Selecionado: animal: "+animal+" id: "+this.animalEdicao.getId());
		System.out.println("nome: "+ this.animalEdicao.getNome());
	}	
	
	public void novoAnimal() {
		System.out.println("Entrou em novoAnimal e animalEdicao = new Animal");
		this.animalEdicao = new Animal();
	}
	
	public void excluirAnimal() {
		
		try{
			String nome = animalSelecionado.getNome();
			int indice = this.proprietario.getAnimais().indexOf(animalSelecionado);
			this.proprietario.getAnimais().remove(indice);
       	    salvar();
       	    
			FacesUtil.addInfoMessage("Animal "+nome+" excluído.");			
		} catch (NegocioException ne) {
			FacesUtil.addErrorMessage("Mensagem21: "+ne.getMessage());
		} catch (Exception e) {
			
			System.out.println("Erro getCause: "+e.getCause().toString());
			FacesUtil.addErrorMessage("Mensagem41: "+e.getMessage().trim());
		} 

	}	
	
	public void cancelaNovoAnimal() {
		System.out.println("cancelaNovoAnimal() chamado");
	}
	
	public void alterarAnimal(){
		if (this.animalEdicao.isAnimalValido()) {		
			salvar();
         	FacesUtil.addInfoMessage("Animal alterado!");
		}
	}
	
	public void adicionarAnimal(){
		if (this.animalEdicao != null) {

			if (this.animalEdicao.isAnimalValido()) {
				
	             this.animalEdicao.setProprietario(this.proprietario);
	          	 this.proprietario.getAnimais().add(this.animalEdicao);
	          	 salvar();

	 			FacesUtil.addInfoMessage("Animal adicionado!");	             
			}
		} else {
			FacesUtil.addInfoMessage("Dados do Animal não informados!");
		}

	}
	
	public void carregaProprietario(){

	}
	
	public void salvar() {

		try {
		
	    	this.proprietario = this.cadastroProprietarioService
						.salvarSemAnimais(this.proprietario);
	    	
	    	//carregaProprietario();//@@@@
	    	
				FacesUtil.addInfoMessage("Proprietário e Animais salvos com sucesso!");

		} catch (NegocioException ne) {
			FacesUtil.addErrorMessage("Mensagem2: "+ne.getMessage());
		} catch (Exception e) {
			
			if (e.getCause().toString().contains("ConstraintViolationException")) {
				FacesUtil.addErrorMessage("Animal não pode ser excluído, pois existem atendimentos para ele");	
			} else {
				FacesUtil.addErrorMessage("Animal não pode ser salvo.: "+e.getCause().toString());				
			}
			
			System.out.println("ErroSalvar getCause: "+e.getCause().toString());
			System.out.println("Erro: "+e.getMessage());
			
		} 
		
	}
	
	public void inicializar() {
		if (FacesUtil.isNotPostback()) {
			this.especies = this.especieRepository.especies();
			if (this.proprietario.getEndereco() == null){
				this.proprietario.setEndereco(new Endereco());
			}
		}
	}
	
	public void carregarRacasPorEspecie(){
		if (this.animalEdicao.getEspecie().getId() != null){
   			this.racas = this.racaRepository.racasPorEspecie(this.animalEdicao.getEspecie().getId());
     	}
	}

	public boolean isEditando() {
		return this.proprietario.getId() != null;
	}

	public boolean isCadastrandoAnimal() {
		return this.animalEdicao != null;
	}

	public boolean isUsuarioVeterinario() {
		return seguranca.getTipoGrupoUsuario().toUpperCase().equals("VETERINARIO");
	}	
	
}
