package br.uece.favet.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

import br.uece.favet.util.jsf.FacesUtil;
import br.uece.favet.util.report.ExecutorRelatorio;

@Named
@RequestScoped
public class RelatorioDadosAnimalBean implements Serializable {

	private static final long serialVersionUID = 1L;


	@Inject
	private FacesContext facesContext;

	@Inject
	private HttpServletResponse response;

	@Inject
	private EntityManager manager;
	
	public void emitir(Long idAnimal) {
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("animal_id", idAnimal);
		
		ExecutorRelatorio executor = new ExecutorRelatorio("/relatorios/prontuario_animal.jasper",
				                            this.response, parametros, "Prontuario_Animal.pdf");
		
//		ExecutorRelatorio executor2 = new ExecutorRelatorio("/relatorios/rel_animal_proprietario.jasper",
//                this.response, parametros, "Prontuario_Animal.pdf");		
		
		
		
		Session session = manager.unwrap(Session.class);
		session.doWork(executor);
		
		if (executor.isRelatorioGerado()) {
			facesContext.responseComplete();
		} else {
			FacesUtil.addErrorMessage("A execução do relatório não retornou dados.");
		}
	}


}
