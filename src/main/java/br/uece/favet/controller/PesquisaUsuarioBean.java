package br.uece.favet.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.uece.favet.model.Usuario;
import br.uece.favet.repository.UsuarioRepository;
import br.uece.favet.repository.filter.UsuarioFilter;

@Named
@ViewScoped
public class PesquisaUsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UsuarioRepository usuarioRepository;

	private UsuarioFilter filtro;

	private Set<Usuario> usuariosFiltrados;

	public PesquisaUsuarioBean() {
		filtro = new UsuarioFilter();
		usuariosFiltrados = new HashSet<Usuario>();
	}

	public void pesquisar() {
		usuariosFiltrados = usuarioRepository.filtrados(filtro);
	}

	public Set<Usuario> getUsuariosFiltrados() {
		return usuariosFiltrados;
	}

	public UsuarioFilter getFiltro() {
		return filtro;
	}

}
