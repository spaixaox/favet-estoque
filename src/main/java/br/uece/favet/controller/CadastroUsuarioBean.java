package br.uece.favet.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.uece.favet.model.Grupo;
import br.uece.favet.model.Usuario;
import br.uece.favet.model.Veterinario;
import br.uece.favet.repository.GrupoRepository;
import br.uece.favet.repository.VeterinarioRepository;
import br.uece.favet.service.CadastroUsuarioService;
import br.uece.favet.service.NegocioException;
import br.uece.favet.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroUsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Grupo> grupos;
	private List<Veterinario> veterinarios;	
	private Usuario usuario;
	
	@Inject
	private CadastroUsuarioService cadastroUsuarioService;
	
	@Inject
	private VeterinarioRepository veterinarioRepository;
	
	@Inject
	private GrupoRepository grupoRepository;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Grupo> getGrupos() {
		return grupos;
	}

	public List<Veterinario> getVeterinarios() {
		return veterinarios;
	}
	
	public CadastroUsuarioBean() {
        limpar();
	}

	public void limpar() {
		usuario = new Usuario();
		System.out.println("Criado novo Usuário no Limpa()");
	}

	public void salvar() {

		try {

			if (this.usuario.validaSenha()){
		    	this.usuario = this.cadastroUsuarioService
							.salvar(this.usuario);
		    	
					FacesUtil.addInfoMessage("Usuário salvo com sucesso!");
			} else {
				FacesUtil.addErrorMessage("Confirmação de senha deve ser igual à senha!");	
			}

		} catch (NegocioException ne) {
			FacesUtil.addErrorMessage("Erro ao Salvar Usuário 1: "+ne.getMessage());
		} 	catch (Exception e){
			FacesUtil.addErrorMessage("Erro ao Salvar Usuário 2: "+e.getMessage()+", Causa :"+e.getCause());
		} 
		
	}
	
	public void inicializar() {
		if (FacesUtil.isNotPostback()) {
			this.grupos = this.grupoRepository.grupos();
			this.veterinarios = this.veterinarioRepository.veterinarios();			
		}
	}
	
	public boolean isEditando() {
		return this.usuario.getId() != null;
	}
	
}
