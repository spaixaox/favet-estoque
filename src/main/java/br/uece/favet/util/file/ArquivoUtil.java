package br.uece.favet.util.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import br.uece.favet.config.ConfiguracaoWeb;
import br.uece.favet.model.enums.ModuloSistema;

public class ArquivoUtil {

	private String getNomeSistema(){
		ConfiguracaoWeb conf = new ConfiguracaoWeb();
		String nomeSistema = conf.getSistemaNome();
		return nomeSistema;
	}

	//Talvez seja interessante ao invés de deletar direto, mover para uma pasta temporária.
	//os arquivos poderão ser excluidos (rotina interna automática) após 1 mês|semana
	public boolean excluirArquivo(String caminhoArquivo) throws IOException{
		Path arquivo = Paths.get(caminhoArquivo);
		return Files.deleteIfExists(arquivo);
	}

	public boolean existeArquivo(String caminhoArquivo){
		Path arquivo = Paths.get(caminhoArquivo);
		return Files.exists(arquivo);
	}	
	
    public List<File> listar(ModuloSistema modulo, Long idAtendimento) {

    	String pathDiretorios = montarPath(modulo, idAtendimento);
    	System.out.println("listar->pathDiretorios: "+pathDiretorios);
    	File dir = diretorioRaizParaArquivos(pathDiretorios);
        //File dir = diretorioRaizParaArquivos(modulo, idAtendimento);

        return Arrays.asList(dir.listFiles());
    }

	public String montarPath(ModuloSistema moduloSistema, Long idAtendimento){
		String path = null;
		String dirNomeApp = null;
		String dirNomeModulo = null;
		String dirIdAtendimento = null;

		if(moduloSistema == ModuloSistema.ATENDIMENTO){
			System.out.println("montarPath: entrou no modulo Atendimento");
	    	dirNomeApp       = getNomeSistema().toLowerCase();
	    	dirNomeModulo    = moduloSistema.name().toLowerCase();
	    	dirIdAtendimento = String.valueOf(idAtendimento);
	    	path = diretorioRaiz()+"/"+dirNomeApp+"/"+dirNomeModulo+"/"+dirIdAtendimento;
	    	//ex: /<raizDoTomCat / UploadsArquivos>/<nome da aplicação>/<nome do módulo>/<id Atendimento> 
	    	//ex: /opt/apache-tomcat/UploadsArquivos/arquivoweb/atendimento/1051/"
		} else {
			System.out.println("montarPath: entrou no modulo não informado, diferente de Atendimento");			
	    	dirNomeApp       = getNomeSistema().toLowerCase();
	    	dirNomeModulo    = moduloSistema.name().toLowerCase();
	    	path = diretorioRaiz()+"/"+dirNomeApp+"/"+dirNomeModulo;
		}
		
		criarEstruturaParaUpload(path);//Cria diretórios		
		
		return path;
	}    

    public File criarEstruturaParaUpload(String path) {
        File dir = new File(path);
        
        System.out.println("criarEstruturaParaUpload->path :"+path);

        if (!dir.exists()) {
            dir.mkdirs();
        }

        return dir;
    }
	
    public File diretorioRaizParaArquivos(String path) {
        File dir = new File(path);
        
        System.out.println("diretorioRaizParaArquivos->path :"+path);

        if (!dir.exists()) {
            dir.mkdirs();
        }

        return dir;
    }

    public File diretorioRaiz() {

         File dir = new File(System.getProperty("catalina.base"), "UploadsArquivos");
         //irá criar(se necessário) e retornar:  /opt/apache-tomcat/UploadsArquivos
         
         //System.getProperty("user.home")
         //System.getProperty("user.dir");         
         //System.getProperty("catalina.base");         
         //System.getProperty("java.io.tmpdir");

        if (!dir.exists()) {
            dir.mkdirs();
        }

        return dir;
    }
	
	
}
