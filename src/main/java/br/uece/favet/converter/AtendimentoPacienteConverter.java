package br.uece.favet.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.uece.favet.model.AtendimentoPaciente;
import br.uece.favet.repository.AtendimentoPacienteRepository;
import br.uece.favet.util.cdi.CDIServiceLocator;

@FacesConverter(forClass = AtendimentoPaciente.class)
public class AtendimentoPacienteConverter implements Converter {

	private AtendimentoPacienteRepository atendimentoPacienteRepository;
	
	public AtendimentoPacienteConverter() {
		atendimentoPacienteRepository = CDIServiceLocator.getBean(AtendimentoPacienteRepository.class);
	}
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		AtendimentoPaciente retorno = null;
		
		if (value != null) {
			Long id = new Long(value);
			retorno = atendimentoPacienteRepository.porId(id);
		}
		
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			AtendimentoPaciente atendimentoPaciente = (AtendimentoPaciente) value;
			return atendimentoPaciente.getId() == null ? null : atendimentoPaciente.getId().toString();
		}
		
		return "";
	}

}
