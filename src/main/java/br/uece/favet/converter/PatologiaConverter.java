package br.uece.favet.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.uece.favet.model.Patologia;
import br.uece.favet.repository.PatologiaRepository;
import br.uece.favet.util.cdi.CDIServiceLocator;

@FacesConverter(forClass = Patologia.class)
public class PatologiaConverter implements Converter {

	//@Inject
	//private PatologiaRepository patologiaRepository;
	private PatologiaRepository patologiaRepository;
	
	public PatologiaConverter() {
		patologiaRepository = CDIServiceLocator.getBean(PatologiaRepository.class);
	}
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Patologia retorno = null;
		
		if (value != null) {
			Long id = new Long(value);
			retorno = patologiaRepository.porId(id);
		}
		
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Patologia patologia = (Patologia) value;
			return patologia.getId() == null ? null : patologia.getId().toString();
		}
		
		return "";
	}

}
