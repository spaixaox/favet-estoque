package br.uece.favet.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.uece.favet.model.Animal;
import br.uece.favet.repository.AnimalRepository;
import br.uece.favet.util.cdi.CDIServiceLocator;

@FacesConverter(forClass = Animal.class)
public class AnimalConverter implements Converter {

	//@Inject
	//private AnimalRepository animalRepository;
	private AnimalRepository animalRepository;
	
	public AnimalConverter() {
		animalRepository = CDIServiceLocator.getBean(AnimalRepository.class);
	}
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Animal retorno = null;
		
		if (value != null) {
			Long id = new Long(value);
			retorno = animalRepository.porId(id);
		}
		
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Animal animal = (Animal) value;
			return animal.getId() == null ? null : animal.getId().toString();
		}
		
		return "";
	}

}
