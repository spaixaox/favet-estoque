package br.uece.favet.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.uece.favet.model.Raca;
import br.uece.favet.repository.RacaRepository;
import br.uece.favet.util.cdi.CDIServiceLocator;

@FacesConverter(forClass = Raca.class)
public class RacaConverter implements Converter {

	//@Inject
	//private RacaRepository racaRepository;
	private RacaRepository racaRepository;
	
	public RacaConverter() {
		racaRepository = CDIServiceLocator.getBean(RacaRepository.class);
	}
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Raca retorno = null;
		
		if (value != null) {
			Long id = new Long(value);
			retorno = racaRepository.porId(id);
		}
		
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Raca raca = (Raca) value;
			return raca.getId() == null ? null : raca.getId().toString();
		}
		
		return "";
	}

}
