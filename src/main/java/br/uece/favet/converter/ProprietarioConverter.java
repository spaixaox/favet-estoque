package br.uece.favet.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.uece.favet.model.Proprietario;
import br.uece.favet.repository.ProprietarioRepository;
import br.uece.favet.util.cdi.CDIServiceLocator;

@FacesConverter(forClass = Proprietario.class)
public class ProprietarioConverter implements Converter {

	//@Inject
	//private ProprietarioRepository proprietarioRepository;
	private ProprietarioRepository proprietarioRepository;
	
	public ProprietarioConverter() {
		proprietarioRepository = CDIServiceLocator.getBean(ProprietarioRepository.class);
	}
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Proprietario retorno = null;
		
		if (value != null) {
			Long id = new Long(value);
			retorno = proprietarioRepository.porId(id);
		}
		
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Proprietario proprietario = (Proprietario) value;
			return proprietario.getId() == null ? null : proprietario.getId().toString();
		}
		
		return "";
	}

}
