package br.uece.favet.teste;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class Teste {

	@Inject
	public static EntityManager manager;
	
	public static void main(String[] args) {
		//EntityManagerFactory factory = null;
		//script();
		
	//	byte[] msg = "FavetUece".getBytes();
		
//		Criptografia cripto = new Criptografia();
      //  String encripto = cripto.criptoA("Zefir#2017");
        //System.out.println("String codificada " + encripto);
        //String decripto = cripto.decriptoA(encripto);
        //System.out.println("String decodificada " + decripto);        
		/*
		byte[] msgCripto = cripto.cripto2(msg);
		System.out.println("Cripto: "+msgCripto+ " String "+new String(msgCripto));		
		byte[] msgDeCripto = cripto.decripto2(msgCripto);
		System.out.println("Decripto: "+new String(msgDeCripto));
		*/
	}
	
	public static void script(){
		EntityTransaction trx  = null;
		
		try {
			//factory = Persistence.createEntityManagerFactory("FavetPU");
			//manager = factory.createEntityManager();
			trx = manager.getTransaction();

			trx.begin();			

			/*
//			ProdutoDeposito prodDep = new ProdutoDeposito();
			Produto prod = new Produto();
			prod.setCodigo("VAC888");
			prod.setNome("Vacina 888");
			
			Calendar dataEmissao = new GregorianCalendar(2017, Calendar.JANUARY, 05);
			prod.setDataAlteracao(dataEmissao.getTime());
			
			UnidadeMedida un1 = new UnidadeMedida();
			un1.setDescricao("Caixa");
			un1.setSigla("CX");			
			prod.setUnidadeMedidaPadrao(un1);
			
			prod.setPermiteVendaFracionada(false);

			Deposito dep = new Deposito();
			dep.setNome("Centro Cirúrgico");
			dep.setUsadaNaEntradaMercadorias(false);
			
	//		prodDep.setProduto(prod);
		//	prodDep.setDeposito(dep);
		//	prodDep.setEstoquePadrao(new Double(1000));
			//prodDep.setDataAtualizacao(dataEmissao.getTime());
			
			//manager.persist(prodDep);
			
			EntradaMercadoria entrada = new EntradaMercadoria();
			
			Calendar data = new GregorianCalendar(2017, Calendar.JANUARY, 10);
			entrada.setDataEntrada(data.getTime());

			Deposito dep = new Deposito();
			dep.setNome("Depósito Central");
			dep = manager.merge(dep);
			entrada.setDeposito(dep);
			
			Fornecedor fornecedor = new Fornecedor();
			fornecedor.setCnpjCpf("65412345699");
			fornecedor.setNomeFantasia("Spaço Chic");
			fornecedor.setRazaoSocial("Spaço Chic");
			fornecedor.setEmail("espacochique@gmail.com");
			fornecedor.setTipoPessoa(TipoPessoa.JURIDICA);
			fornecedor = manager.merge(fornecedor);			
			entrada.setFornecedor(fornecedor);

			entrada.setStatus(StatusEntrada.ABERTO);
			entrada.setValorTotal(new BigDecimal(1200.00));
			entrada.setNumeroDocFiscal(1234);
			
			Calendar dataEmissao = new GregorianCalendar(2017, Calendar.JANUARY, 05);
			entrada.setDataEmissaoDocFiscal(dataEmissao.getTime());
			
			entrada.setSerieDocFiscal("01");
			entrada.setModeloDocFiscal("55");
			
			//Preparando Itens para as Entradas de Mercadorias
			Date dataHoje = new Date(System.currentTimeMillis());

			
			List<ItemEntradaMercadoria> itens = new ArrayList<>();
			
			ItemEntradaMercadoria item1 = new ItemEntradaMercadoria();
			
			Produto prod1 = new Produto();
			prod1.setDataCriacao(dataHoje);
			prod1.setCodigo("VEST456");
			prod1.setNome("Vestido Lindo 36");
			
			UnidadeMedida un1 = new UnidadeMedida();
			un1.setDescricao("Unidade");
			un1.setSigla("UN");
			un1 = manager.merge(un1);
			
			prod1.setUnidadeMedidaPadrao(un1);
			item1.setUnidadeMedida(un1);
			
			prod1 = manager.merge(prod1);			
			
			item1.setProduto(prod1);
			item1.setQuantidade(new Double(30));
			item1.setEntradaMercadoria(entrada);
			
			itens.add(item1);
			
			//Item 2
			ItemEntradaMercadoria item2 = new ItemEntradaMercadoria();
			
			Produto prod2 = new Produto();
			prod2.setDataCriacao(dataHoje);
			prod2.setCodigo("VAC999");
			prod2.setNome("Vacina ABC");
			
			UnidadeMedida un2 = new UnidadeMedida();
			un2.setDescricao("Mililitro");
			un2.setSigla("ML");
			un2 = manager.merge(un2);
			
			prod2.setUnidadeMedidaPadrao(un2);
			item2.setUnidadeMedida(un2);
			
			prod2 = manager.merge(prod2);			
			
			item2.setProduto(prod2);
			item2.setQuantidade(new Double(15));
			item2.setEntradaMercadoria(entrada);
			
			itens.add(item2);
			
			entrada.setItens(itens);
			manager.persist(entrada);

  */
			trx.commit();


		} finally {
			manager.close();
		//	factory.close();
		}
	}
		
}
