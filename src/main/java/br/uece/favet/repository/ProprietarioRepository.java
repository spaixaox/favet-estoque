package br.uece.favet.repository;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.uece.favet.model.Proprietario;
import br.uece.favet.repository.filter.ProprietarioFilter;
import br.uece.favet.service.NegocioException;
import br.uece.favet.util.jpa.Transactional;

public class ProprietarioRepository implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	@SuppressWarnings("unchecked")
	public Set<Proprietario> filtrados(ProprietarioFilter filtro) {

		Session session = this.manager.unwrap(Session.class);

		Criteria criteria = session.createCriteria(Proprietario.class);

		if (StringUtils.isNotBlank(filtro.getNomeProprietario())) {
			criteria.add(Restrictions.ilike("nome",
					filtro.getNomeProprietario(), MatchMode.ANYWHERE));
		}

	    if (StringUtils.isNotBlank(filtro.getCpfCnpj())) {
			 criteria.add(Restrictions.eq("cpfCnpj",
			 filtro.getCpfCnpj())); 
		}
	    
		return new LinkedHashSet<Proprietario>(criteria.addOrder(Order.asc("nome")).list());
	}
	
	
	public Proprietario porId(Long id) {
		return this.manager.find(Proprietario.class, id);
	}

	public Proprietario guardar(Proprietario proprietario) {
		return this.manager.merge(proprietario);
	}
	
	@Transactional
	public void remover(Proprietario proprietario){
		try {
			proprietario = porId(proprietario.getId());
			//o  remove() prepara o objeto para exclusão, será excluído quando for 
			//chamado o commit da transação ou quando for chamado um flush();
			manager.remove(proprietario);
			//o flush() efetiva todas as persistências pendentes. Está sendo chamado aqui para 
			//podermos tratar exceções caso haja algum animal sendo usado em atendimento
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Proprietário não pode ser excluído.: "+e);
		}
	}
	
}
