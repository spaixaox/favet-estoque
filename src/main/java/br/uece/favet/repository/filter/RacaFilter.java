package br.uece.favet.repository.filter;

import java.io.Serializable;

public class RacaFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nomeRaca;
	private String nomeEspecie;

	public String getNomeRaca() {
		return nomeRaca;
	}
	public void setNomeRaca(String nomeRaca) {
		this.nomeRaca = nomeRaca;
	}
	public String getNomeEspecie() {
		return nomeEspecie;
	}
	public void setNomeEspecie(String nomeEspecie) {
		this.nomeEspecie = nomeEspecie;
	}	
}
