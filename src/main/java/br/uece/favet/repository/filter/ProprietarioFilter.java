package br.uece.favet.repository.filter;

import java.io.Serializable;

public class ProprietarioFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private String cpfCnpj;
	private String nomeProprietario;
	private String codigoAnimal;
	private String codigoLegadoAnimal;
	private String nomeAnimal;

	public String getCpfCnpj() {
		return cpfCnpj;
	}
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public String getNomeProprietario() {
		return nomeProprietario;
	}
	public void setNomeProprietario(String nomeProprietario) {
		this.nomeProprietario = nomeProprietario;
	}
	public String getCodigoAnimal() {
		return codigoAnimal;
	}
	public void setCodigoAnimal(String codigoAnimal) {
		this.codigoAnimal = codigoAnimal;
	}
	public String getCodigoLegadoAnimal() {
		return codigoLegadoAnimal;
	}
	public void setCodigoLegadoAnimal(String codigoLegadoAnimal) {
		this.codigoLegadoAnimal = codigoLegadoAnimal;
	}
	public String getNomeAnimal() {
		return nomeAnimal;
	}
	public void setNomeAnimal(String nomeAnimal) {
		this.nomeAnimal = nomeAnimal;
	}	
}
