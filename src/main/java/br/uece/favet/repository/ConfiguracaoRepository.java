package br.uece.favet.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.uece.favet.model.Configuracao;

public class ConfiguracaoRepository implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;
	
	public Configuracao porId(Long id) {
		return this.manager.find(Configuracao.class, id);
	}
	
	public List<Configuracao> configuracoes(){
		return this.manager.createQuery("from Configuracao order by chave", Configuracao.class)
				.getResultList();
	}

	public Configuracao porChave(String chave) {
			return this.manager.createQuery("from Configuracao where upper(chave) = :chave", Configuracao.class)
					.setParameter("chave", chave.toUpperCase())
					.getSingleResult();
	}
}
