package br.uece.favet.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.uece.favet.model.UploadArquivo;

public class UploadArquivoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;
	
	public UploadArquivo guardar(UploadArquivo uploadArquivo) {
		return this.manager.merge(uploadArquivo);
	}

	public UploadArquivo porId(Long id) {
		return this.manager.find(UploadArquivo.class, id);
	}

	public List<UploadArquivo> porAtendimento(Long idAtendimento) {
		return this.manager.createQuery("from UploadArquivo where atendimentoPaciente.id = :idAtendimento order by id", UploadArquivo.class)
				.setParameter("idAtendimento", idAtendimento)
				.getResultList();
	}
}
