package br.uece.favet.repository;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.uece.favet.model.Veterinario;
import br.uece.favet.repository.filter.VeterinarioFilter;

public class VeterinarioRepository implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Veterinario guardar(Veterinario veterinario){
		return this.manager.merge(veterinario);
	}
	
	public Veterinario porId(Long id) {
		return this.manager.find(Veterinario.class, id);
	}

	@SuppressWarnings("unchecked")
	public Set<Veterinario> filtrados(VeterinarioFilter filtro) {

		Session session = this.manager.unwrap(Session.class);

		Criteria criteria = session.createCriteria(Veterinario.class);

	    if (StringUtils.isNotBlank(filtro.getCrmv() )) {
			 criteria.add(Restrictions.eq("crmv",
			 filtro.getCrmv() )); 
		}
		
		if (StringUtils.isNotBlank(filtro.getNome() )) {
			criteria.add(Restrictions.ilike("nome",
					filtro.getNome(), MatchMode.ANYWHERE));
		}
		
		return new LinkedHashSet<Veterinario>(criteria.addOrder(Order.asc("nome")).list());
	}
	
	
	public List<Veterinario> veterinarios(){
		return this.manager.createQuery("from Veterinario order by nome", Veterinario.class)
				.getResultList();
	}	

}
