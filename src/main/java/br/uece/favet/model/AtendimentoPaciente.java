package br.uece.favet.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;

import br.uece.favet.model.enums.StatusAtendimentoPaciente;
import br.uece.favet.model.enums.TipoAtendimentoPaciente;
import br.uece.favet.util.jsf.FacesUtil;

@Entity
@Table(name="atendimento_paciente")
public class AtendimentoPaciente implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "atendimento_paciente_id_seq")
	@SequenceGenerator(name = "atendimento_paciente_id_seq", sequenceName = "atendimento_paciente_id_seq", allocationSize = 1)	
	private Long id;
	
	@Column(length = 10)
	private String senha;
	
	@Column(nullable=true)
	private boolean prioritario = false;

	//@NotNull
	@ManyToOne
	@JoinColumn(name = "usuario_criou_id", foreignKey = @ForeignKey(name = "fk_atendimento_paciente__usuario__usuario_criou_id"))	
	private Usuario usuarioCriou;
	
	@ManyToOne
	@JoinColumn(name = "usuario_iniciou_id", foreignKey = @ForeignKey(name = "fk_atendimento_paciente__usuario__usuario_iniciou_id"))	
	private Usuario usuarioIniciou;

	@ManyToOne
	@JoinColumn(name = "usuario_finalizou_id", foreignKey = @ForeignKey(name = "fk_atendimento_paciente__usuario__usuario_finalizou_id"))	
	private Usuario usuarioFinalizou;	

	@ManyToOne
	@JoinColumn(name = "usuario_atualizou_id", foreignKey = @ForeignKey(name = "fk_atendimento_paciente__usuario__usuario_atualizou_id"))	
	private Usuario usuarioAtualizou;	
	
	@ManyToOne
	@JoinColumn(name = "patologia_id", foreignKey = @ForeignKey(name = "fk_atendimento_paciente__patologia__patologia_id"))	
	private Patologia patologia;	
	
	@Column(name="outra_patologia", length = 100)
	private String outraPatologia;//patologia principal	

	@Column(nullable = true, length = 50)
	private String resumo;//poucas palavras para aparecer no Histórico
	
	@NotNull
	//@ManyToOne(cascade=CascadeType.MERGE)
	@ManyToOne
	@JoinColumn(name = "animal_id", nullable = false, foreignKey = @ForeignKey(name = "fk_atendimento_paciente__animal__animal_id"))	
	private Animal animal;

	@Column(nullable = true)
	private Double peso;
	
	//@NotNull
	@ManyToOne
	@JoinColumn(name = "veterinario_id", nullable=true, foreignKey = @ForeignKey(name = "fk_atendimento_paciente__veterinario__veterinario_id"))	
	private Veterinario veterinario;

	//@NotNull
	@ManyToOne
	@JoinColumn(name = "consultorio_id", nullable=true, foreignKey = @ForeignKey(name = "fk_atendimento_paciente__consultorio__consultorio_id"))	
	private Consultorio consultorio;		

	//@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data_criacao")	
	private Date dataCriacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_criacao")	
	private Date dataHoraCriacao;
	
	//@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_atualizacao")	
	private Date dataHoraAtualizacao;
	
	//@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_inicio")	
	private Date dataHoraInicio;
	
	//@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_fim")	
	private Date dataHoraFim;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_cancelamento")	
	private Date dataHoraCancelamento;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 20)
	private StatusAtendimentoPaciente status = StatusAtendimentoPaciente.ESPERA;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name="tipo_atendimento_paciente", nullable = false, length = 20)
	private TipoAtendimentoPaciente tipoAtendimentoPaciente = TipoAtendimentoPaciente.CLINICO;	

	@Column(columnDefinition = "text")
	private String anamnese; 	

	@Column(columnDefinition = "text")
	private String diagnostico;

	@Column(name = "exame_complementar", length=255)
	private String exameComplementar;
	
	@Column(name = "exame_complementar2",columnDefinition = "text")
	private String exameComplementar2;	
	
	@Column(name = "resultado_exame_complementar", length=255)	
	private String resultadoExameComplementar;

	@Column(name = "resultado_exame_complementar2",columnDefinition = "text")
	private String resultadoExameComplementar2;	
	
	@Column(columnDefinition = "text")
	private String tratamento;	
	
	@Column(name = "tratamento_cirurgico",columnDefinition = "text")
	private String tratamentoCirurgico;	
	
	@Column(name = "exame_fisico", columnDefinition = "text")
	private String exameFisico;	

	@Column(columnDefinition = "text")
	private String receituario;
	
	@Column(columnDefinition = "text")
	private String observacao;
	
	@Column(name = "laudo_ultrassom", columnDefinition = "text")
	private String laudoUltrassom; 		

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSenha() {
		return senha;
	}
	
	public boolean isPrioritario() {
		return prioritario;
	}

	public void setPrioritario(boolean prioritario) {
		this.prioritario = prioritario;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Usuario getUsuarioCriou() {
		return usuarioCriou;
	}

	public void setUsuarioCriou(Usuario usuarioCriou) {
		this.usuarioCriou = usuarioCriou;
	}

	public Usuario getUsuarioIniciou() {
		return usuarioIniciou;
	}

	public void setUsuarioIniciou(Usuario usuarioIniciou) {
		this.usuarioIniciou = usuarioIniciou;
	}

	public Usuario getUsuarioFinalizou() {
		return usuarioFinalizou;
	}

	public void setUsuarioFinalizou(Usuario usuarioFinalizou) {
		this.usuarioFinalizou = usuarioFinalizou;
	}

	public Usuario getUsuarioAtualizou() {
		return usuarioAtualizou;
	}

	public void setUsuarioAtualizou(Usuario usuarioAtualizou) {
		this.usuarioAtualizou = usuarioAtualizou;
	}

	public Patologia getPatologia() {
		return patologia;
	}

	public void setPatologia(Patologia patologia) {
		this.patologia = patologia;
	}

	public String getOutraPatologia() {
		return outraPatologia;
	}

	public void setOutraPatologia(String outraPatologia) {
		this.outraPatologia = outraPatologia;
	}

	public String getResumo() {
		return resumo;
	}

	public void setResumo(String resumo) {
		this.resumo = resumo;
	}

	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public Consultorio getConsultorio() {
		return consultorio;
	}

	public void setConsultorio(Consultorio consultorio) {
		this.consultorio = consultorio;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDataHoraCriacao() {
		return dataHoraCriacao;
	}

	public void setDataHoraCriacao(Date dataHoraCriacao) {
		this.dataHoraCriacao = dataHoraCriacao;
	}

	public Date getDataHoraAtualizacao() {
		return dataHoraAtualizacao;
	}

	public void setDataHoraAtualizacao(Date dataHoraAtualizacao) {
		this.dataHoraAtualizacao = dataHoraAtualizacao;
	}

	public Date getDataHoraInicio() {
		return dataHoraInicio;
	}

	public void setDataHoraInicio(Date dataHoraInicio) {
		this.dataHoraInicio = dataHoraInicio;
	}

	public Date getDataHoraFim() {
		return dataHoraFim;
	}

	public void setDataHoraFim(Date dataHoraFim) {
		this.dataHoraFim = dataHoraFim;
	}
	
	public Date getDataHoraCancelamento() {
		return dataHoraCancelamento;
	}

	public void setDataHoraCancelamento(Date dataHoraCancelamento) {
		this.dataHoraCancelamento = dataHoraCancelamento;
	}

	public StatusAtendimentoPaciente getStatus() {
		return status;
	}

	public void setStatus(StatusAtendimentoPaciente status) {
		this.status = status;
	}

	public TipoAtendimentoPaciente getTipoAtendimentoPaciente() {
		return tipoAtendimentoPaciente;
	}

	public void setTipoAtendimentoPaciente(
			TipoAtendimentoPaciente tipoAtendimentoPaciente) {
		this.tipoAtendimentoPaciente = tipoAtendimentoPaciente;
	}

	public String getAnamnese() {
		return anamnese;
	}

	public void setAnamnese(String anamnese) {
		this.anamnese = anamnese;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}
	
	public String getExameComplementar() {
		return exameComplementar;
	}

	public void setExameComplementar(String exameComplementar) {
		this.exameComplementar = exameComplementar;
	}

	public String getExameComplementar2() {
		return exameComplementar2;
	}

	public void setExameComplementar2(String exameComplementar2) {
		this.exameComplementar2 = exameComplementar2;
	}
	
	public String getResultadoExameComplementar() {
		return resultadoExameComplementar;
	}

	public void setResultadoExameComplementar(String resultadoExameComplementar) {
		this.resultadoExameComplementar = resultadoExameComplementar;
	}
	
	public String getResultadoExameComplementar2() {
		return resultadoExameComplementar2;
	}

	public void setResultadoExameComplementar2(String resultadoExameComplementar2) {
		this.resultadoExameComplementar2 = resultadoExameComplementar2;
	}

	public String getTratamento() {
		return tratamento;
	}

	public void setTratamento(String tratamento) {
		this.tratamento = tratamento;
	}

	public String getTratamentoCirurgico() {
		return tratamentoCirurgico;
	}

	public void setTratamentoCirurgico(String tratamentoCirurgico) {
		this.tratamentoCirurgico = tratamentoCirurgico;
	}
	
	public String getExameFisico() {
		return exameFisico;
	}

	public void setExameFisico(String exameFisico) {
		this.exameFisico = exameFisico;
	}

	public String getReceituario() {
		return receituario;
	}

	public void setReceituario(String receituario) {
		this.receituario = receituario;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public String getLaudoUltrassom() {
		return laudoUltrassom;
	}

	public void setLaudoUltrassom(String laudoUltrassom) {
		this.laudoUltrassom = laudoUltrassom;
	}

	@Transient
	public boolean isNovo(){
		return getId() == null;
	}

	@Transient	
	public boolean isExistente(){
		return !isNovo();
	}

	@Transient	
	public boolean isEmAtendimento(){
		return this.status == StatusAtendimentoPaciente.ATENDIMENTO;
	}
	
	@Transient	
	public boolean isEmEspera(){
		return this.status == StatusAtendimentoPaciente.ESPERA;
	}	
    
	@Transient	
	public boolean isFinalizado(){
		return this.status == StatusAtendimentoPaciente.ATENDIDO;
	}
    
	@Transient	
	public boolean isCancelado(){
		return this.status == StatusAtendimentoPaciente.CANCELADO;
	}
	
	@Transient
	public String getNomePrioritario(){
		return this.prioritario ? "Prioritário" : "";
	}

	@Transient	
	public boolean isTipoAtendimentoCirurgico(){
		return this.tipoAtendimentoPaciente == TipoAtendimentoPaciente.CIRURGICO ||
			   this.tipoAtendimentoPaciente == TipoAtendimentoPaciente.AVALIACAO_CIRURGICA ||
			   this.tipoAtendimentoPaciente == TipoAtendimentoPaciente.RETORNO_CIRURGICO;
	}
	
	@Transient	
	public boolean isTipoAtendimentoExame(){
		return this.tipoAtendimentoPaciente == TipoAtendimentoPaciente.EXAME;
	}

	@Transient	
	public boolean isTipoAtendimentoClinico(){
		return this.tipoAtendimentoPaciente == TipoAtendimentoPaciente.CLINICO;
	}	

	@Transient	
	public boolean isTipoAtendimentoRetorno(){
		return this.tipoAtendimentoPaciente == TipoAtendimentoPaciente.RETORNO;
	}		
	
	@Transient	
	public boolean isPodeIniciar(){
	  boolean pode = true;
	  
		if (this.animal == null ) {
			pode = false;
			FacesUtil.addErrorMessage("O animal deve ser informado.");
		} else {
            if (this.veterinario == null){
				pode = false;
				FacesUtil.addErrorMessage("O Dr(a). veterinário(a) deve ser informado(a).");
            } else {
                if (this.consultorio == null){
					pode = false;
					FacesUtil.addErrorMessage("O consultório deve ser informado.");
                } 
            } 
		}

		return pode;
	}
	
	
	@Transient	
	public boolean isPodeFinalizar(){
	  boolean pode = true;
	  
		if (this.getDataHoraInicio() == null) {
			pode = false;
			FacesUtil.addErrorMessage("O atendimento deve ser inicializado.");
		} else {
			if (this.animal == null ) {
				pode = false;
				FacesUtil.addErrorMessage("O animal deve ser informado informado.");
			} else {
                if (this.veterinario == null){
					pode = false;
					FacesUtil.addErrorMessage("O Dr(a). veterinário(a) deve ser informado(a).");
                } else {
                    if (this.consultorio == null){
    					pode = false;
    					FacesUtil.addErrorMessage("O consultório deve ser informado.");
                    } else {
        				if (StringUtils.isBlank(this.anamnese)) {
        					pode = false;
        					FacesUtil.addErrorMessage("Anamnese deve ser informada.");
        				} /* else {
        					
        					if (StringUtils.isBlank(this.resumo)){
            					pode = false;
            					FacesUtil.addErrorMessage("Resumo deve ser informado.");        						
        					}
        				}*/
                    }
                } 
			}
		}
			return pode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((anamnese == null) ? 0 : anamnese.hashCode());
		result = prime * result + ((animal == null) ? 0 : animal.hashCode());
		result = prime * result
				+ ((consultorio == null) ? 0 : consultorio.hashCode());
		result = prime * result
				+ ((dataCriacao == null) ? 0 : dataCriacao.hashCode());
		result = prime
				* result
				+ ((dataHoraAtualizacao == null) ? 0 : dataHoraAtualizacao
						.hashCode());
		result = prime
				* result
				+ ((dataHoraCancelamento == null) ? 0 : dataHoraCancelamento
						.hashCode());
		result = prime * result
				+ ((dataHoraCriacao == null) ? 0 : dataHoraCriacao.hashCode());
		result = prime * result
				+ ((dataHoraFim == null) ? 0 : dataHoraFim.hashCode());
		result = prime * result
				+ ((dataHoraInicio == null) ? 0 : dataHoraInicio.hashCode());
		result = prime * result
				+ ((diagnostico == null) ? 0 : diagnostico.hashCode());
		result = prime
				* result
				+ ((exameComplementar == null) ? 0 : exameComplementar
						.hashCode());
		result = prime
				* result
				+ ((exameComplementar2 == null) ? 0 : exameComplementar2
						.hashCode());
		result = prime * result
				+ ((exameFisico == null) ? 0 : exameFisico.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((laudoUltrassom == null) ? 0 : laudoUltrassom.hashCode());
		result = prime * result
				+ ((observacao == null) ? 0 : observacao.hashCode());
		result = prime * result
				+ ((outraPatologia == null) ? 0 : outraPatologia.hashCode());
		result = prime * result
				+ ((patologia == null) ? 0 : patologia.hashCode());
		result = prime * result + ((peso == null) ? 0 : peso.hashCode());
		result = prime * result + (prioritario ? 1231 : 1237);
		result = prime * result
				+ ((receituario == null) ? 0 : receituario.hashCode());
		result = prime
				* result
				+ ((resultadoExameComplementar == null) ? 0
						: resultadoExameComplementar.hashCode());
		result = prime
				* result
				+ ((resultadoExameComplementar2 == null) ? 0
						: resultadoExameComplementar2.hashCode());
		result = prime * result + ((resumo == null) ? 0 : resumo.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime
				* result
				+ ((tipoAtendimentoPaciente == null) ? 0
						: tipoAtendimentoPaciente.hashCode());
		result = prime * result
				+ ((tratamento == null) ? 0 : tratamento.hashCode());
		result = prime
				* result
				+ ((tratamentoCirurgico == null) ? 0 : tratamentoCirurgico
						.hashCode());
		result = prime
				* result
				+ ((usuarioAtualizou == null) ? 0 : usuarioAtualizou.hashCode());
		result = prime * result
				+ ((usuarioCriou == null) ? 0 : usuarioCriou.hashCode());
		result = prime
				* result
				+ ((usuarioFinalizou == null) ? 0 : usuarioFinalizou.hashCode());
		result = prime * result
				+ ((usuarioIniciou == null) ? 0 : usuarioIniciou.hashCode());
		result = prime * result
				+ ((veterinario == null) ? 0 : veterinario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtendimentoPaciente other = (AtendimentoPaciente) obj;
		if (anamnese == null) {
			if (other.anamnese != null)
				return false;
		} else if (!anamnese.equals(other.anamnese))
			return false;
		if (animal == null) {
			if (other.animal != null)
				return false;
		} else if (!animal.equals(other.animal))
			return false;
		if (consultorio == null) {
			if (other.consultorio != null)
				return false;
		} else if (!consultorio.equals(other.consultorio))
			return false;
		if (dataCriacao == null) {
			if (other.dataCriacao != null)
				return false;
		} else if (!dataCriacao.equals(other.dataCriacao))
			return false;
		if (dataHoraAtualizacao == null) {
			if (other.dataHoraAtualizacao != null)
				return false;
		} else if (!dataHoraAtualizacao.equals(other.dataHoraAtualizacao))
			return false;
		if (dataHoraCancelamento == null) {
			if (other.dataHoraCancelamento != null)
				return false;
		} else if (!dataHoraCancelamento.equals(other.dataHoraCancelamento))
			return false;
		if (dataHoraCriacao == null) {
			if (other.dataHoraCriacao != null)
				return false;
		} else if (!dataHoraCriacao.equals(other.dataHoraCriacao))
			return false;
		if (dataHoraFim == null) {
			if (other.dataHoraFim != null)
				return false;
		} else if (!dataHoraFim.equals(other.dataHoraFim))
			return false;
		if (dataHoraInicio == null) {
			if (other.dataHoraInicio != null)
				return false;
		} else if (!dataHoraInicio.equals(other.dataHoraInicio))
			return false;
		if (diagnostico == null) {
			if (other.diagnostico != null)
				return false;
		} else if (!diagnostico.equals(other.diagnostico))
			return false;
		if (exameComplementar == null) {
			if (other.exameComplementar != null)
				return false;
		} else if (!exameComplementar.equals(other.exameComplementar))
			return false;
		if (exameComplementar2 == null) {
			if (other.exameComplementar2 != null)
				return false;
		} else if (!exameComplementar2.equals(other.exameComplementar2))
			return false;
		if (exameFisico == null) {
			if (other.exameFisico != null)
				return false;
		} else if (!exameFisico.equals(other.exameFisico))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (laudoUltrassom == null) {
			if (other.laudoUltrassom != null)
				return false;
		} else if (!laudoUltrassom.equals(other.laudoUltrassom))
			return false;
		if (observacao == null) {
			if (other.observacao != null)
				return false;
		} else if (!observacao.equals(other.observacao))
			return false;
		if (outraPatologia == null) {
			if (other.outraPatologia != null)
				return false;
		} else if (!outraPatologia.equals(other.outraPatologia))
			return false;
		if (patologia == null) {
			if (other.patologia != null)
				return false;
		} else if (!patologia.equals(other.patologia))
			return false;
		if (peso == null) {
			if (other.peso != null)
				return false;
		} else if (!peso.equals(other.peso))
			return false;
		if (prioritario != other.prioritario)
			return false;
		if (receituario == null) {
			if (other.receituario != null)
				return false;
		} else if (!receituario.equals(other.receituario))
			return false;
		if (resultadoExameComplementar == null) {
			if (other.resultadoExameComplementar != null)
				return false;
		} else if (!resultadoExameComplementar
				.equals(other.resultadoExameComplementar))
			return false;
		if (resultadoExameComplementar2 == null) {
			if (other.resultadoExameComplementar2 != null)
				return false;
		} else if (!resultadoExameComplementar2
				.equals(other.resultadoExameComplementar2))
			return false;
		if (resumo == null) {
			if (other.resumo != null)
				return false;
		} else if (!resumo.equals(other.resumo))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		if (status != other.status)
			return false;
		if (tipoAtendimentoPaciente != other.tipoAtendimentoPaciente)
			return false;
		if (tratamento == null) {
			if (other.tratamento != null)
				return false;
		} else if (!tratamento.equals(other.tratamento))
			return false;
		if (tratamentoCirurgico == null) {
			if (other.tratamentoCirurgico != null)
				return false;
		} else if (!tratamentoCirurgico.equals(other.tratamentoCirurgico))
			return false;
		if (usuarioAtualizou == null) {
			if (other.usuarioAtualizou != null)
				return false;
		} else if (!usuarioAtualizou.equals(other.usuarioAtualizou))
			return false;
		if (usuarioCriou == null) {
			if (other.usuarioCriou != null)
				return false;
		} else if (!usuarioCriou.equals(other.usuarioCriou))
			return false;
		if (usuarioFinalizou == null) {
			if (other.usuarioFinalizou != null)
				return false;
		} else if (!usuarioFinalizou.equals(other.usuarioFinalizou))
			return false;
		if (usuarioIniciou == null) {
			if (other.usuarioIniciou != null)
				return false;
		} else if (!usuarioIniciou.equals(other.usuarioIniciou))
			return false;
		if (veterinario == null) {
			if (other.veterinario != null)
				return false;
		} else if (!veterinario.equals(other.veterinario))
			return false;
		return true;
	}

}
