package br.uece.favet.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.uece.favet.model.enums.ModuloSistema;

@Entity
@Table(name = "upload_arquivo")
public class UploadArquivo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "upload_arquivo_id_seq")
	@SequenceGenerator(name = "upload_arquivo_id_seq", sequenceName = "upload_arquivo_id_seq", allocationSize = 1)	
	Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_upload")	
	private Date dataHoraUpload;

	@ManyToOne
	@JoinColumn(name = "usuario_id", nullable = true, foreignKey = @ForeignKey(name = "fk_upload_arquivo_usuario_usuario_id"))	
	private Usuario usuario;
	
	@Column(name="content_type", length = 30)
	private String contentType;
	
	//PostgreSql aceita identificadores até 63 Bytes (63 caracteres)
	//https://www.postgresql.org/docs/9.3/static/sql-syntax-lexical.html
	@ManyToOne
	@JoinColumn(name = "atendimento_paciente_id", nullable = true, foreignKey = @ForeignKey(name = "fk_upload_arquivo_atendimento_paciente_atendimento_paciente_id"))
	private AtendimentoPaciente atendimentoPaciente;
	
	@Enumerated(EnumType.STRING)
	@Column(name="modulo_sistema", nullable = false, length = 30)
	private ModuloSistema moduloSistema;

	@Column(length = 50)
	private String descricao;
	
	@Column(name="nome_origem")
	private String nomeOrigem;

	@Column(name="nome_destino")
	private String nomeDestino;	
	
	@Column(name="caminho_diretorio")
	private String caminhoDiretorio;	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_exclusao")	
	private Date dataHoraExclusao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataHoraUpload() {
		return dataHoraUpload;
	}

	public void setDataHoraUpload(Date dataHoraUpload) {
		this.dataHoraUpload = dataHoraUpload;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public AtendimentoPaciente getAtendimentoPaciente() {
		return atendimentoPaciente;
	}

	public void setAtendimentoPaciente(AtendimentoPaciente atendimentoPaciente) {
		this.atendimentoPaciente = atendimentoPaciente;
	}

	public ModuloSistema getModuloSistema() {
		return moduloSistema;
	}

	public void setModuloSistema(ModuloSistema moduloSistema) {
		this.moduloSistema = moduloSistema;
	}
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNomeOrigem() {
		return nomeOrigem;
	}

	public void setNomeOrigem(String nomeOrigem) {
		this.nomeOrigem = nomeOrigem;
	}

	public String getNomeDestino() {
		return nomeDestino;
	}

	public void setNomeDestino(String nomeDestino) {
		this.nomeDestino = nomeDestino;
	}

	public String getCaminhoDiretorio() {
		return caminhoDiretorio;
	}

	public void setCaminhoDiretorio(String caminhoDiretorio) {
		this.caminhoDiretorio = caminhoDiretorio;
	}

	public Date getDataHoraExclusao() {
		return dataHoraExclusao;
	}

	public void setDataHoraExclusao(Date dataHoraExclusao) {
		this.dataHoraExclusao = dataHoraExclusao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((atendimentoPaciente == null) ? 0 : atendimentoPaciente
						.hashCode());
		result = prime
				* result
				+ ((caminhoDiretorio == null) ? 0 : caminhoDiretorio.hashCode());
		result = prime * result
				+ ((contentType == null) ? 0 : contentType.hashCode());
		result = prime
				* result
				+ ((dataHoraExclusao == null) ? 0 : dataHoraExclusao.hashCode());
		result = prime * result
				+ ((dataHoraUpload == null) ? 0 : dataHoraUpload.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((moduloSistema == null) ? 0 : moduloSistema.hashCode());
		result = prime * result
				+ ((nomeDestino == null) ? 0 : nomeDestino.hashCode());
		result = prime * result
				+ ((nomeOrigem == null) ? 0 : nomeOrigem.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UploadArquivo other = (UploadArquivo) obj;
		if (atendimentoPaciente == null) {
			if (other.atendimentoPaciente != null)
				return false;
		} else if (!atendimentoPaciente.equals(other.atendimentoPaciente))
			return false;
		if (caminhoDiretorio == null) {
			if (other.caminhoDiretorio != null)
				return false;
		} else if (!caminhoDiretorio.equals(other.caminhoDiretorio))
			return false;
		if (contentType == null) {
			if (other.contentType != null)
				return false;
		} else if (!contentType.equals(other.contentType))
			return false;
		if (dataHoraExclusao == null) {
			if (other.dataHoraExclusao != null)
				return false;
		} else if (!dataHoraExclusao.equals(other.dataHoraExclusao))
			return false;
		if (dataHoraUpload == null) {
			if (other.dataHoraUpload != null)
				return false;
		} else if (!dataHoraUpload.equals(other.dataHoraUpload))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (moduloSistema != other.moduloSistema)
			return false;
		if (nomeDestino == null) {
			if (other.nomeDestino != null)
				return false;
		} else if (!nomeDestino.equals(other.nomeDestino))
			return false;
		if (nomeOrigem == null) {
			if (other.nomeOrigem != null)
				return false;
		} else if (!nomeOrigem.equals(other.nomeOrigem))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}
}
