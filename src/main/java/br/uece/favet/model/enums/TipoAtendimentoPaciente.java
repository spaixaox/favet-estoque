package br.uece.favet.model.enums;

public enum TipoAtendimentoPaciente {
	CLINICO("Clínico"), 
	RETORNO("Retorno Clínico"),
	CIRURGICO("Cirúrgico"),
    AVALIACAO_CIRURGICA("Avaliação Cirúrgica"),
    RETORNO_CIRURGICO("Retorno Cirúrgico"),    
	EXAME("Exame Ultrassom");
		
	private String descricao;
	
	TipoAtendimentoPaciente(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
