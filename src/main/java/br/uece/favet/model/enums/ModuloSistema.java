package br.uece.favet.model.enums;

public enum ModuloSistema {
	ATENDIMENTO("Atendimento");
		
	private String descricao;
	
	ModuloSistema(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
    
}
