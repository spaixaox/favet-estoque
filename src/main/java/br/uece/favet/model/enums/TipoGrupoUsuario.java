package br.uece.favet.model.enums;

public enum TipoGrupoUsuario {
	ADMINISTRADOR("Administrador"), 
	ASSISTENTE_FARMACIA("Assistente de Farmácia"),
	ASSISTENTE_ATENDIMENTO("Assistente de Atendimento"),
	VETERINARIO("Veterinário"),
	GESTOR("Gestor");	
	
	private String descricao;
	
	TipoGrupoUsuario(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}

