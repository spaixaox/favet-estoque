package br.uece.favet.model.enums;

public enum TipoPorte {
	PEQUENO("Pequeno"), 
	MEDIO("Médio"),
	GRANDE("Grande"),
	INDEFINIDO("Indefinido");	
	
	private String descricao;
	
	TipoPorte(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
