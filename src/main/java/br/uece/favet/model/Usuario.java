package br.uece.favet.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.uece.favet.model.enums.TipoGrupoUsuario;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator="usuario_id_seq")
    @SequenceGenerator(name="usuario_id_seq", sequenceName="usuario_id_seq", allocationSize=1)	
	private Long id;
	// @NotBlank
	@Column(nullable = false, length = 80)	
	private String nome;

	@Column(length = 20)	
	private String login;
	
	@Column(nullable = false, unique = true, length = 100)	
	private String email;
	@Column(nullable = false, length = 50)	
	private String senha;

	@Transient
	private String senhaConfirmacao;
	
	@OneToOne
	@JoinColumn(name = "veterinario_id", nullable=true, foreignKey = @ForeignKey(name = "fk_usuario__veterinario__veterinario_id"))	
	private Veterinario veterinario;

	@ManyToOne
	@JoinColumn(name = "grupo_id", nullable=false, foreignKey = @ForeignKey(name = "fk_usuario__grupo__grupo_id"))	
	private Grupo grupo;

	@Column(nullable=true)
	private Boolean redefinir = false;
	
	@Column(nullable=true)
	private Boolean ativo = true;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_alteracao")	
	private Date dataHoraAlteracao;

	@Column(name="pode_iniciar_atendimento", nullable=true)
	private Boolean podeIniciarAtendimento = false;	
	
	public boolean validaSenha(){
		 return this.senha.equals(this.senhaConfirmacao);
	}

	@Transient
	public String getStatus(){
		return this.ativo ? "Atvo" : "Inativo";
	}
	
	@Transient
	public boolean isNaoEhVeterinario(){
		return this.grupo.getTipoGrupoUsuario() != TipoGrupoUsuario.VETERINARIO;
	}	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSenhaConfirmacao() {
		return senhaConfirmacao;
	}

	public void setSenhaConfirmacao(String senhaConfirmacao) {
		this.senhaConfirmacao = senhaConfirmacao;
	}

	public Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	
	public Boolean getRedefinir() {
		return redefinir;
	}

	public void setRedefinir(Boolean redefinir) {
		this.redefinir = redefinir;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Date getDataHoraAlteracao() {
		return dataHoraAlteracao;
	}

	public void setDataHoraAlteracao(Date dataHoraAlteracao) {
		this.dataHoraAlteracao = dataHoraAlteracao;
	}

	public Boolean getPodeIniciarAtendimento() {
		return podeIniciarAtendimento;
	}

	public void setPodeIniciarAtendimento(Boolean podeIniciarAtendimento) {
		this.podeIniciarAtendimento = podeIniciarAtendimento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ativo == null) ? 0 : ativo.hashCode());
		result = prime
				* result
				+ ((dataHoraAlteracao == null) ? 0 : dataHoraAlteracao
						.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((grupo == null) ? 0 : grupo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime
				* result
				+ ((podeIniciarAtendimento == null) ? 0
						: podeIniciarAtendimento.hashCode());
		result = prime * result
				+ ((redefinir == null) ? 0 : redefinir.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		result = prime
				* result
				+ ((senhaConfirmacao == null) ? 0 : senhaConfirmacao.hashCode());
		result = prime * result
				+ ((veterinario == null) ? 0 : veterinario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (ativo == null) {
			if (other.ativo != null)
				return false;
		} else if (!ativo.equals(other.ativo))
			return false;
		if (dataHoraAlteracao == null) {
			if (other.dataHoraAlteracao != null)
				return false;
		} else if (!dataHoraAlteracao.equals(other.dataHoraAlteracao))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (grupo == null) {
			if (other.grupo != null)
				return false;
		} else if (!grupo.equals(other.grupo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (podeIniciarAtendimento == null) {
			if (other.podeIniciarAtendimento != null)
				return false;
		} else if (!podeIniciarAtendimento.equals(other.podeIniciarAtendimento))
			return false;
		if (redefinir == null) {
			if (other.redefinir != null)
				return false;
		} else if (!redefinir.equals(other.redefinir))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		if (senhaConfirmacao == null) {
			if (other.senhaConfirmacao != null)
				return false;
		} else if (!senhaConfirmacao.equals(other.senhaConfirmacao))
			return false;
		if (veterinario == null) {
			if (other.veterinario != null)
				return false;
		} else if (!veterinario.equals(other.veterinario))
			return false;
		return true;
	}

}
