package br.uece.favet.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="vacina")
public class Vacina implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator="vacina_id_seq")
    @SequenceGenerator(name="vacina_id_seq", sequenceName="vacina_id_seq", allocationSize=1)	
	private Long id;

	@Column(length=50, nullable=false)
	private String nome;

	@Column(length=150)	
	private String descricao;

	@ManyToOne
	@JoinColumn(name = "especie_id", foreignKey = @ForeignKey(name = "fk_vacina__especie__especie_id"))	
	private Especie especie;
	
	@Column(name="numero_doses")
	private int numeroDoses = 0;

	@Column(name="dias_intervalo_entre_doses")
	private int diasIntervaloEntreDoses = 0; //dias de intervalo entre doses

	@Column(name="revacinacao_anual")	
	private boolean revacinacaoAnual = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public int getNumeroDoses() {
		return numeroDoses;
	}

	public void setNumeroDoses(int numeroDoses) {
		this.numeroDoses = numeroDoses;
	}

	public int getDiasIntervaloEntreDoses() {
		return diasIntervaloEntreDoses;
	}

	public void setDiasIntervaloEntreDoses(int diasIntervaloEntreDoses) {
		this.diasIntervaloEntreDoses = diasIntervaloEntreDoses;
	}

	public boolean isRevacinacaoAnual() {
		return revacinacaoAnual;
	}

	public void setRevacinacaoAnual(boolean revacinacaoAnual) {
		this.revacinacaoAnual = revacinacaoAnual;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + diasIntervaloEntreDoses;
		result = prime * result + ((especie == null) ? 0 : especie.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + numeroDoses;
		result = prime * result + (revacinacaoAnual ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vacina other = (Vacina) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (diasIntervaloEntreDoses != other.diasIntervaloEntreDoses)
			return false;
		if (especie == null) {
			if (other.especie != null)
				return false;
		} else if (!especie.equals(other.especie))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (numeroDoses != other.numeroDoses)
			return false;
		if (revacinacaoAnual != other.revacinacaoAnual)
			return false;
		return true;
	}

}
