package br.uece.favet.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="veterinario")
public class Veterinario implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator="veterinario_id_seq")
    @SequenceGenerator(name="veterinario_id_seq", sequenceName="veterinario_id_seq", allocationSize=1)
	private Long id;
	@Column(nullable = false, length = 80)
	private String nome;
	@Column(length = 20)	
	private String crmv;
	@Column(length = 100)	
	private String email;
	@Column(length = 15)	
	private String telefone;
	@Column(length = 15)	
	private String celular;
	
	//TODO: analisar remoção do Usuario da classe Veterinario
	@ManyToOne
	@JoinColumn(name = "usuario_id", nullable = true, foreignKey = @ForeignKey(name = "fk_veterinario__usuario__usuario_id"))
	private Usuario usuario;

	private boolean ativo = true;
	
	@Column(nullable = true)	
	private Boolean cirurgiao = false;
	
	@Column(nullable = true)	
	private Boolean clinico = true;
	
	@Column(name="faz_exames", nullable = true)	
	private Boolean fazExames = false;  	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getcrmv() {
		return crmv;
	}
	public void setcrmv(String cRMV) {
		crmv = cRMV;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	public boolean isCirurgiao() {
		return cirurgiao;
	}
	public void setCirurgiao(boolean cirurgiao) {
		this.cirurgiao = cirurgiao;
	}
	
	public Boolean getClinico() {
		return clinico;
	}
	public void setClinico(Boolean clinico) {
		this.clinico = clinico;
	}
	public Boolean getFazExames() {
		return fazExames;
	}
	public void setFazExames(Boolean fazExames) {
		this.fazExames = fazExames;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((crmv == null) ? 0 : crmv.hashCode());
		result = prime * result + (ativo ? 1231 : 1237);
		result = prime * result + ((celular == null) ? 0 : celular.hashCode());
		result = prime * result
				+ ((cirurgiao == null) ? 0 : cirurgiao.hashCode());
		result = prime * result + ((clinico == null) ? 0 : clinico.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((fazExames == null) ? 0 : fazExames.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result
				+ ((telefone == null) ? 0 : telefone.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Veterinario other = (Veterinario) obj;
		if (crmv == null) {
			if (other.crmv != null)
				return false;
		} else if (!crmv.equals(other.crmv))
			return false;
		if (ativo != other.ativo)
			return false;
		if (celular == null) {
			if (other.celular != null)
				return false;
		} else if (!celular.equals(other.celular))
			return false;
		if (cirurgiao == null) {
			if (other.cirurgiao != null)
				return false;
		} else if (!cirurgiao.equals(other.cirurgiao))
			return false;
		if (clinico == null) {
			if (other.clinico != null)
				return false;
		} else if (!clinico.equals(other.clinico))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (fazExames == null) {
			if (other.fazExames != null)
				return false;
		} else if (!fazExames.equals(other.fazExames))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

}
