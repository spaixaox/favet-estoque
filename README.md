Partindo de um sistema já existente (Atendimento Veterinário), este projeto tem a proposta de desenvolver mecanismos de Controle de Estoque de produtos de um Hospital Veterinário. Suas principais funcionalidades serão:

- Cadastro de Produto;
- Mapeamento de Conversão de Unidades (um produto poderá ser comprado em Caixa ou Unidade e ser dado baixa em ml (mililitro);
- Entrada de Mercadorias;
- Transferência de Estoques (suporta vários estoques);
- Manutenção de Mercadorias (por avaria, para incineração, ajuste após inventários); e
- Atendimento de Medicamento (baixa das quantidades utilizadas por atendimento de animais);

Este projeto permanecerá com as mesmas tecnologias do sistema de Atendimento: JavaEE, JSF 2.2, Primefaces 6.0, JPA 2.0 com Hibernate, CDI (IoC/DI), JasperSoft (relatórios .jasper), PostgreSql 9.3.